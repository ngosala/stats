/*
Validate Events by looking at the datastore. The following conditions are validated
> shirt must be put on before hat
> shoe must be put on after socks and pants
> hat is optional but every other clothing is required to get dressed
*/
const validateAndProcessEvents = function(events, inputEvent, eventSeq) {
    /*
    In case of empty store or intially
    */
    if (eventSeq.isEmpty()) {
        /*
        User cannot put on hat or shoe and leave home without meeting above criteria
        */
        if (inputEvent === 1 || inputEvent === 4 || inputEvent === 6) {
            eventSeq.addToList(7);
            return "invalidSequence";
        }
        /*
        If it's shirt or socks or pant, proceed with processing
        If it's unknown event (number not in events), skip
        */
        else if ((events[inputEvent] !== undefined)) {
            eventSeq.addToList(inputEvent);
            return;
        }
        /*
        In case of bad input or input not in event mapping, fail
        */
        else {
            eventSeq.addToList(7);
            return "invalidSequence";
        }
    }
    /*
    If user already put on clothing, validate the requirements for subsequent
    events
    */
    else {
        /*
        If user wants to put on hat, user should have worn shirt first, else fail
        */
        if (inputEvent === 1) {
            if (!(eventSeq.findANode(1)) && (eventSeq.findANode(3))) {
                eventSeq.addToList(inputEvent);
                return;
            }
        }
        /*
        If user wants to put on shoe, user should have worn pants and socks
        socks and pants can be worn in any order
        */
        else if (inputEvent === 4) {
            if (!(eventSeq.findANode(4)) && (eventSeq.findMultipleNodes([2, 5]))) {
                //Requirement met
                eventSeq.addToList(inputEvent);
                return;
            }
        }
        /*
        If user is ready to leave
        */
        else if (inputEvent === 6) {
            /*
            Validate if dressed and ready to leave
            condition: Hat is Optional but rest all are necessary
            */
            if (eventSeq.findMultipleNodes([2, 3, 4, 5])) {
                eventSeq.addToList(inputEvent);
                return;
            }
        }
        /*
        If all above conditions are not satisfied, and if the input event belongs
        to event mapping, then add to event store
        */
        else {
            if ((!eventSeq.findANode(inputEvent)) && (events[inputEvent] !== undefined)) {
                eventSeq.addToList(inputEvent);
                return;
            }
        }
        /*
        In any other scenario, input event is not valid and should fail
        */
        eventSeq.addToList(7);
        return "invalidSequence";
    }
};

/*
  Function to confirm if dressed.
  Oputput is formatted using "," as separator and is written in a single line
  of stdout.
  Result is also stored in an array and is returned
*/
function confirmIfDressed(events, printOutput, eventSeq) {
    /*
    result of operations
    */
    let resultOps = [];

    /*
    If user doesn't decide to leave or if input doesn't contain 6,
    then return fail
    For events like wear shirt, wear hat, exit etc.
    */
    if (!eventSeq.findANode(6) && !eventSeq.findANode(7)) {
        eventSeq.addToList(7);
    }

    /*
    To format output properly with "," separation, first element is printed
    separately
    */
    let currentelement = eventSeq.head;
    let nextElement = currentelement.next;

    /*
    Store and print mapped data of corresponding first element.
    This essemtially prints the event itself instead of number
    */
    resultOps.push(events[currentelement.value]);
    /*
    A flag to skip printing during Tests
    */
    if (printOutput)
        process.stdout.write(events[currentelement.value]);

    /*
    Iterate over store and print remaining processed events
    */
    while (nextElement !== null) {
        /*
        Store and print mapped data of corresponding elements.
        This essemtially prints the event itself instead of number
        separated by comma
        */
        resultOps.push(events[nextElement.value]);
        /*
        A flag to skip printing during Tests
        */
        if (printOutput)
            process.stdout.write("," + events[nextElement.value]);

        /*
        To iterate over the datastore
        */
        currentelement = nextElement;
        nextElement = currentelement.next;
    }
    return resultOps;

};

/*
Export functions
*/
module.exports = {
    validateAndProcessEvents,
    confirmIfDressed
};
