/*To read from stdin*/
const readline = require('readline');
/*Import helper functions*/
const { validateAndProcessEvents, confirmIfDressed } = require('./operations');
/* Import Event Data Store*/
const EventStore = require('./eventStore');
/*Event mappings*/
const events = {
    1: "hat",
    2: "pants",
    3: "shirt",
    4: "shoes",
    5: "socks",
    6: "leave",
    7: "fail"
};

// Initialise readline interface
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

/*
Function to match regex for input (numbers separated by comma)
and process input
*/
const readAndprocessEvents = function() {
    /*readline on line input*/
    rl.on('line', (input) => {
        /*
        Regex to match only numbers separated by comma
        In case of bad input or null input, return immediately throwing error
        */
        if (input.match(/^([0-9]{1}([,]{1})?)+$/g) !== null) {
            /*
            process given Events. Input to processEvents is array of events in order
            */
            processEvents(input.split(','), true);
        } else {
            console.log("Given input appears to be wrongly formatted: " + input);
        }
        /*close stream after reading a line*/
        rl.close();
    });

};

function processEvents(inputArray, printOutput) {
    /*
    Instantiate datastore object
    Instantiate new Datastore for each processEvent or each user
    */
    const eventSeq = new EventStore();
    /*
    Using regular for loop to make use of break statement to stop process
    in case of error
    */
    for (let i = 0; i < inputArray.length; i++) {
        if (validateAndProcessEvents(events, parseInt(inputArray[i]), eventSeq) === "invalidSequence") {
            break;
        }
    }
    /*
    After processing Events, return output
    */
    return confirmIfDressed(events, printOutput, eventSeq);
}

/* Execute function to read and process events*/
readAndprocessEvents();

/* Export processEvents functions for unittesting */
module.exports = processEvents;
