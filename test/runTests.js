const chai = require('chai');
const assert = chai.assert;
const processEvents = require('../run');

describe('Test Getting Dressed', function() {
  describe('Positive Test Scenarios', function() {

    it('should return correct sequence with socks anr pants before', function(done) {
      assert.deepEqual(["socks", "pants", "shirt", "shoes", "hat", "leave"], processEvents([5,2,3,4,1,6], false));
      done();
    });

    it('should return correct sequence without hat', function(done) {
      assert.deepEqual(["pants", "socks", "shoes", "shirt", "leave"], processEvents([2,5,4,3,6], false));
      done();
    });

    it('should return correct sequenc with socks and pants interchanged', function(done) {
      assert.deepEqual(["socks", "pants", "shirt", "shoes", "leave"], processEvents([5,2,3,4,6], false));
      done();
    });

    it('should return correct sequence with all operations', function(done) {
      assert.deepEqual(["shirt", "hat", "pants", "socks", "shoes", "leave"], processEvents([3,1,2,5,4,6], false));
      done();
    });

  });

  describe('negative Test Scenarios', function() {


    it('should return fail if not dressed', function(done) {
      assert.deepEqual(["fail"], processEvents([6], false));
      done();
    });

    it('should return fail trying to wear hat before shirt', function(done) {
      assert.deepEqual(["fail"], processEvents([1,3,6], false));
      done();
    });

    it('should return fail if user tries to wear shoes before socks', function(done) {
      assert.deepEqual(["shirt", "hat", "pants", "fail"], processEvents([3,1,2,4,5,6], false));
      done();
    });

    it('should return fail if user tries to wear shoes before pant', function(done) {
      assert.deepEqual(["shirt", "socks", "fail"], processEvents([3,5,4,2,6], false));
      done();
    });

    it('should return fail if user tries to select invalid data', function(done) {
      assert.deepEqual(["fail"], processEvents([8], false));
      done();
    });

    it('should return fail if user forgets to leave', function(done) {
      assert.deepEqual(["socks", "pants", "shirt", "shoes", "hat", "fail"], processEvents([5,2,3,4,1], false));
      done();
    });

    it('should return fail if user forgets anything other than hat', function(done) {
      assert.deepEqual(["socks", "pants", "shoes", "fail"], processEvents([5,2,4,6], false));
      done();
    });

    it('should return fail if user tries to wear same clothing again', function(done) {
      assert.deepEqual(["socks", "pants", "shoes", "shirt", "fail"], processEvents([5,2,4,3,3,6], false));
      done();
    });

  });
});
