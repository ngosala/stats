# README

The above program executes the sequence of events (user trying to wear clothes)
and validates them against below conditions

1. shirt must be put on before hat
2. shoe must be put on after socks and pants
3. hat is optional but every other clothing is required to get dressed

## INSTALLATION & RUN
Project is written in Node.JS

```bash
npm install

npm run.js
```

##INPUT
comma separated numbers like 5,1,6 via stdin

```bash
npm run.js
5,2,3,4,1,6
socks,pants,shirt,shoes,hat,leave
```

##TEST
Run mocha tests using the below command
```bash
npm install

npm test
```
