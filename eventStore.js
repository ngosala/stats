/*
Class Eventstore is of type Linked list. This stores the list of events in sequence
*/
module.exports = class EventStore {
    /*
    Constructor to create and Initialise Datastructure
    */
    constructor(node) {
        this.head = {
            value: node,
            next: null
        };
        this.length = 0;
    }

    /*
    Add node to head of linked list
    */
    addToHead(node) {
        const newNode = {
            value: node,
            next: null
        };
        this.head = newNode;
        this.length++;
        return this;
    }

    /*
    Add to the end of linked list
    */
    addToList(node) {
        if (this.isEmpty()) {
            this.addToHead(node);
        } else {
            /*iterate to the end of linked list and add element*/
            const newNode = {
                value: node,
                next: null
            };
            let prevNode = this.head;
            let nextNode = prevNode.next;
            while (nextNode !== null) {
                prevNode = nextNode;
                nextNode = nextNode.next;
            }
            prevNode.next = newNode;
            this.length++;
        }
    }

    /*
    Iterate over linked list and return true if an element is present or else
    return false
    */
    findANode(node) {
        let count = 1; //as we're starting at next element
        let found = 0;
        if (this.length === 0) {
            return null;
        }
        /*
        If node is at head
        */
        else if (this.head.value === node) {
            return this.head;
        }
        /*
        Else iterate over list and print
        */
        else {
            let prevNode = this.head;
            let nextNode = prevNode.next;
            /*
            The first case is covered by above else if condition, hence head is
            not checked
            */
            while (nextNode !== null) {
                count++;
                if (nextNode.value === node) {
                    return true;
                    found = 1;
                    break;
                }
                prevNode = nextNode;
                nextNode = nextNode.next;
            }
            if (found === 0) {
                return false;
            }
        }
    }

    /*
    Find if multiple nodes (array of nodes) exist in list
    return true if all nodes are present in list. Else it returns false
    */
    findMultipleNodes(nodeList) {
        let result = true;
        const self = this;
        if (nodeList.length === 0)
            return false;
        nodeList.forEach(function(node) {
            result = result && self.findANode(node);
        })
        return result;
    }

    isEmpty() {
        return this.length === 0;
    }

}
